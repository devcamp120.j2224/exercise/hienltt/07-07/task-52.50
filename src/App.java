import java.util.ArrayList;
import java.util.Date;

import com.devcamp.j03_javabasic.s50.Order;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> arrayList = new ArrayList<>();
        //Khởi tạo các object order
        Order order1 = new Order();
        Order order2 = new Order("Lan");
        Order order3 = new Order(3, "Long", 80000);
        Order order4 = new Order(4, "Nam", 70000, new Date(), false, new String[]{"hop mau", "tay", "giay mau"});
        //Thêm các object order vào araylist
        arrayList.add(order1);
        arrayList.add(order2);
        arrayList.add(order3);
        arrayList.add(order4);

        //In ra màn hình
        for(Order order : arrayList){
            System.out.println(order.toString());
        }
    }
}
