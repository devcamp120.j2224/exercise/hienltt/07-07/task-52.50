package com.devcamp.j03_javabasic.s50;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class Order {
    int id; //id của order
    String customerName; //tên khách hàng
    long price; //tổng giá tiền
    Date orderDate; //ngày thực hiện order
    boolean confirm; //đã xác nhận hya chưa?
    String[] items; //danh sách mặt hàng đã mua

    // Khởi tạo với 1 tham số
    public Order(String customerName){
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"Book", "pen", "rule"};
    }
    //Khởi tạo với tất cả tham số
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items){
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
    }
    //Khởi tạo không tham số
    public Order(){
        this("2");
    }
    //Khởi tạo với 3 tham số
    public Order(int id, String customerName, long price){
        this(id, customerName, price, new Date(), true, new String[]{"Trung", "Ca", "Dau", "Rau"});
    }

    @Override
    public String toString(){
        // Đinh dạng tiêu chuẩn VN
        Locale.setDefault(new Locale("vi", "VN"));
        // Định dạng cho ngày tháng
        String pattern = "dd-MMMM-yyy HH:mm:ss.SSS";
        DateTimeFormatter defaulTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        // Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        //Trả về chuỗi
        return "Order [id=" + id
            + ", customerName=" + customerName
            + ", price=" + usNumberFormat.format(price)
            + ", orderDate=" + defaulTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
            + ", confirm=" + confirm
            + ", items=" + items;
    }
}
